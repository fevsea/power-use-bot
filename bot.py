from telegram.ext import Updater
import telegram
import os
import logging
from sensor import get_sensor_data
import plotGen
from io import BytesIO

def init_bot():
    updater = Updater(token=os.environ['TEL_BOT_KEY'])

    dispatcher = updater.dispatcher


    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                         level=logging.INFO)

    def start(bot, update):
        bot.send_message(chat_id=update.message.chat_id, text="I'm a bot, please talk to me!")
        custom_keyboard = [['/status', '/plot']]
        reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard, one_time_keyboard=False)
        bot.send_message(chat_id=update.message.chat_id, text="Select an option", reply_markup=reply_markup)

    def status(bot, update):
        data = get_sensor_data()
        text = ""
        for key, value in data.items():
            text += key[6:].capitalize() + ": _" + str(value) + "_\n"
        bot.send_message(chat_id=update.message.chat_id, text=text, parse_mode="MARKDOWN")

    def plot(bot, update):
        buf = plotGen.get_plot()
        buf.seek(0)
        bot.send_photo(update.message.chat_id, photo=buf)
        buf.close()


    from telegram.ext import CommandHandler
    start_handler  = CommandHandler('start', start)
    status_handler = CommandHandler('status', status)
    help_handler   = CommandHandler('help', start)
    plot_handler   = CommandHandler('plot', plot)
    dispatcher.add_handler(start_handler)
    dispatcher.add_handler(status_handler)
    dispatcher.add_handler(help_handler)
    dispatcher.add_handler(plot_handler)

    return updater

