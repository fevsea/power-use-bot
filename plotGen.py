from datetime import datetime, timedelta
import persistence
import seaborn as sns; sns.set()
import matplotlib.pyplot as plt
import io
from PIL import Image

def get_plot():
    d = datetime.today() - timedelta(days=1)
    df = persistence.get_data(d)
    ax = sns.lineplot(x="datetime", y="fase1_p_activa", data = df)
    ax.set(ylim=(0, None))
    ax.set_title("Potencia activa")
    ax.tick_params(axis="x", labelrotation=45)
    buf = io.BytesIO()
    plt.savefig(buf, format='png')
    return buf
