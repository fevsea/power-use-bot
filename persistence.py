import sqlite3
from datetime import datetime, timedelta

import pandas as pd

db_name = "sensor.sqlite3"

def save_record(data):
    conn = sqlite3.connect(db_name)
    c1 = conn.cursor()

    var_def =""
    for key, value in data.items():
        var_def += key + " real, "
    var_def += "datetime DATETIME DEFAULT CURRENT_TIMESTAMP"
    sql = "CREATE TABLE IF NOT EXISTS sensor (" + var_def + ");"
    # Create table
    c1.execute(sql)

    c2 = conn.cursor()
    placeholders = ', '.join('?' * len(data))
    columns = ', '.join(data.keys())
    sql = "INSERT INTO sensor ( %s ) VALUES ( %s )" % (columns, placeholders)
    c2.execute(sql, tuple(data.values()))
    conn.commit()
    conn.close()

def get_data(times):
    conn = sqlite3.connect(db_name)
    sql = '''SELECT *
             FROM sensor
             WHERE datetime >= ?
             ORDER BY datetime;'''
    df = pd.read_sql(sql, conn, params=(times,))
    df['datetime'] = pd.to_datetime(df['datetime'])  + timedelta(hours=1)
    conn.close()
    return df

