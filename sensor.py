import xml.etree.ElementTree as ET
import time
import requests

ip = "192.168.1.40"

def get_sensor_data():
    r = requests.get("http://" + ip + "/en/status.xml")
    if r.status_code != 200:
        raise requests.HTTPError
    tree = ET.ElementTree(ET.fromstring(r.text))
    readings = {}
    for e in tree.iter():
        if e.tag.startswith("fase1"):
            readings[e.tag] = float(e.text)
    return readings

