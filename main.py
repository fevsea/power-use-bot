import threading
from sensor import get_sensor_data
from bot import init_bot
import persistence, plotGen

def update_data():
    t = threading.Timer(5.0, update_data)
    persistence.save_record(get_sensor_data())
    t.start()

t = threading.Timer(5.0, update_data)
t.start()


updater = init_bot()
updater.start_polling()